<?php

/**
 * Remove seo url
 *
 * @author mh
 * @created Fri, 17 Jan 2020 9:26:00 +0200
 */

use JTL\Update\IMigration;
use JTL\Update\Migration;

/**
 * Class Migration_20200117092600
 */
class Migration_20200117092600 extends Migration implements IMigration
{
    protected $author      = 'mh';
    protected $description = 'Remove seo url';

    public function up()
    {
        $redirect = new Redirect();
        while ($url = $this->fetchOne("SELECT `cSeo` FROM `tseo` WHERE `cKey` = 'kSuchanfrage'")) {
            $redirect->saveExt('/' . $url->cSeo, 'index.php?qs=' . $url->cSeo, true);
            $this->execute("DELETE FROM `tseo` WHERE `cKey` = 'kSuchanfrage' AND `cSeo` = '" . $url->cSeo . "'");
        }
    }

    public function down()
    {

    }
}
