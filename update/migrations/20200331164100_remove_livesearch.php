<?php

/**
 * Remove livesearch
 *
 * @author mh
 * @created Tue, 31 Mar 2020 16:41:00 +0200
 */

use JTL\Update\IMigration;
use JTL\Update\Migration;

/**
 * Class Migration_20200331164100
 */
class Migration_20200331164100 extends Migration implements IMigration
{
    protected $author      = 'mh';
    protected $description = 'Remove livesearch';

    public function up()
    {
        //remove LINKTYP_LIVESUCHE
        $this->execute('DELETE FROM `tspezialseite` WHERE `nLinkart` = 15');
        $this->execute("DELETE `tlink`, `tlinkgroupassociations`, `tseo`
                          FROM `tlink`
                          LEFT JOIN `tlinkgroupassociations`
                            ON tlink.kLink = tlinkgroupassociations.linkID 
                          LEFT JOIN `tseo`
                            ON tlink.kLink = tseo.kKey AND tseo.cKey = 'kLink'
                          WHERE tlink.nLinkart = 15"
        );
        //remove PAGE_LIVESUCHE
        $this->execute('DELETE FROM `tboxensichtbar` WHERE `kSeite` = 23');
        $this->execute('DELETE FROM `tboxenanzeige` WHERE `nSeite` = 23');
        $this->execute('DELETE FROM `textensionpoint` WHERE `nSeite` = 23');
        //remove BOX_SUCHWOLKE
        $this->execute('DELETE `tboxvorlage`, `tboxen`, `tboxensichtbar`
                          FROM `tboxvorlage`
                          LEFT JOIN `tboxen`
                            ON tboxen.kBoxvorlage = tboxvorlage.kBoxvorlage
                          LEFT JOIN `tboxensichtbar`
                            ON tboxen.kBox = tboxensichtbar.kBox
                          WHERE tboxvorlage.kBoxvorlage = 13'
        );

        $this->removeConfig('configgroup_8_box_searchcloud');
        $this->removeConfig('boxen_livesuche_count');
        $this->removeConfig('boxen_livesuche_anzeigen');
    }

    public function down()
    {
        $this->execute("INSERT INTO `tspezialseite` VALUES (11,0,'Livesuche','',15,15)");
        $this->execute("INSERT INTO `tboxvorlage`   VALUES (13, 0, 'tpl', 'Suchwolke', '0', 'box_search_cloud.tpl')");

        $this->setConfig(
            'configgroup_8_box_searchcloud',
            'Suchwolke',
            \CONF_BOXEN,
            'Suchwolke',
            null,
            900,
            (object)['cConf' => 'N']
        );
        $this->setConfig(
            'boxen_livesuche_count',
            'Y',
            \CONF_BOXEN,
            'Anzahl angezeigte Suchbegriffe',
            'number',
            910,
            (object)['cBeschreibung' => 'Soviele Begriffe werden in der Suchwolke angezeigt.']
        );
        $this->setConfig(
            'boxen_livesuche_anzeigen',
            'Y',
            \CONF_BOXEN,
            'Box anzeigen',
            'selectbox',
            905,
            (object)[
                'cBeschreibung' => 'Soll die Suchwolke in einer Box angezeigt werden?',
                'inputOptions'  => [
                    'Y' => 'Ja',
                    'N' => 'Nein'
                ]
            ]
        );
    }
}
