[//]: # ( $Id: gitlab_vars.md,v 0.4 2020/06/03 06:24:32 tron5 Exp tron5 $  )
[//]: # ( vim: set fdm=marker )



## push to branch
-----------------
{{{
O - `codeQuality`
O - `cleanUpCodeQuality` (skipped)
O - `test` (7.2/7.3)
O - `cleanUpTest` (7.2/7.3) (skipped)

```
CI_COMMIT_BRANCH                    : CICD-005
CI_COMMIT_REF_NAME                  : CICD-005
CI_MERGE_REQUEST_IID                :
CI_MERGE_REQUEST_ID                 :
CI_COMMIT_TAG                       :
CI_PIPELINE_SOURCE                  : push
CI_MERGE_REQUEST_EVENT_TYPE         :
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME :
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
CI_DEFAULT_BRANCH                   : master
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
```
}}}
check against NULL:
```
CI_COMMIT_BRANCH                    : CICD-025
CI_COMMIT_REF_NAME                  : CICD-025
CI_PIPELINE_SOURCE                  : push
CI_DEFAULT_BRANCH                   : master
```


## (UI) create MR
-----------------
{{{
[branch-pipeline, mr-pipeline (detached)]
O - `code_quality`
O - `removeContainerCondeQuality` (skipped)
O - `test_PHP_7.2` ..`7.3`
O - `removeContainerPHP7.2` ..`7.3` (skipped)

```
CI_COMMIT_BRANCH                    :
CI_COMMIT_REF_NAME                  : CICD-005
CI_MERGE_REQUEST_IID                : 5
CI_MERGE_REQUEST_ID                 : 59588756
CI_COMMIT_TAG                       :
CI_PIPELINE_SOURCE                  : merge_request_event
CI_MERGE_REQUEST_EVENT_TYPE         : detached
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME : CICD-005
CI_MERGE_REQUEST_TARGET_BRANCH_NAME : master
CI_DEFAULT_BRANCH                   : master
CI_MERGE_REQUEST_TARGET_BRANCH_NAME : master
```
}}}
check against NULL:
```
CI_COMMIT_REF_NAME                  : CICD-025
CI_MERGE_REQUEST_IID                : 23
CI_MERGE_REQUEST_ID                 : 59909017
CI_PIPELINE_SOURCE                  : merge_request_event
CI_MERGE_REQUEST_EVENT_TYPE         : detached
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME : CICD-025
CI_MERGE_REQUEST_TARGET_BRANCH_NAME : master
CI_DEFAULT_BRANCH                   : master
```

## push to branch with MR
-------------------------
{{{
```
CI_COMMIT_BRANCH                    :
CI_COMMIT_REF_NAME                  : CICD-008
CI_MERGE_REQUEST_IID                : 8
CI_MERGE_REQUEST_ID                 : 59634404
CI_COMMIT_TAG                       :
CI_PIPELINE_SOURCE                  : merge_request_event
CI_MERGE_REQUEST_EVENT_TYPE         : detached
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME : CICD-008
CI_MERGE_REQUEST_TARGET_BRANCH_NAME : master
CI_DEFAULT_BRANCH                   : master
CI_MERGE_REQUEST_TARGET_BRANCH_NAME : master
```
}}}
check against NULL: (detached)
```
CI_COMMIT_REF_NAME                  : CICD-025
CI_MERGE_REQUEST_IID                : 23
CI_MERGE_REQUEST_ID                 : 59909017
CI_PIPELINE_SOURCE                  : merge_request_event
CI_MERGE_REQUEST_EVENT_TYPE         : detached
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME : CICD-025
CI_MERGE_REQUEST_TARGET_BRANCH_NAME : master
CI_DEFAULT_BRANCH                   : master
```
check against NULL: (against branch)
```
CI_COMMIT_BRANCH                    : CICD-025
CI_COMMIT_REF_NAME                  : CICD-025
CI_PIPELINE_SOURCE                  : push
CI_DEFAULT_BRANCH                   : master
```


## (UI) merge (detached)
------------------------
{{{
stagingPreDeploy
removeContainerPHP7.2 ..7.3
removeContainerCodeQuality

```
CI_COMMIT_BRANCH                    : master
CI_COMMIT_REF_NAME                  : master
CI_MERGE_REQUEST_IID                :
CI_MERGE_REQUEST_ID                 :
CI_COMMIT_TAG                       :
CI_PIPELINE_SOURCE                  : push
CI_MERGE_REQUEST_EVENT_TYPE         :
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME :
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
CI_DEFAULT_BRANCH                   : master
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
```
}}}
check against NULL:
```
CI_COMMIT_BRANCH                    : master
CI_COMMIT_REF_NAME                  : master
CI_PIPELINE_SOURCE                  : push
CI_DEFAULT_BRANCH                   : master
```



## push directly to master
--------------------------
{{{
```
CI_COMMIT_BRANCH                    : master
CI_COMMIT_REF_NAME                  : master
CI_MERGE_REQUEST_IID                :
CI_MERGE_REQUEST_ID                 :
CI_COMMIT_TAG                       :
CI_PIPELINE_SOURCE                  : push
CI_MERGE_REQUEST_EVENT_TYPE         :
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME :
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
CI_DEFAULT_BRANCH                   : master
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
```
}}}
check against NULL:
```
CI_COMMIT_BRANCH                    : master
CI_COMMIT_REF_NAME                  : master
CI_PIPELINE_SOURCE                  : push
CI_DEFAULT_BRANCH                   : master
```




## (UI) create tag
------------------

{{{
```
CI_COMMIT_BRANCH                    :
CI_COMMIT_REF_NAME                  : cicd-1.0.0
CI_MERGE_REQUEST_IID                :
CI_MERGE_REQUEST_ID                 :
CI_COMMIT_TAG                       : cicd-1.0.0
CI_PIPELINE_SOURCE                  : push
CI_MERGE_REQUEST_EVENT_TYPE         :
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME :
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
CI_DEFAULT_BRANCH                   : master
CI_MERGE_REQUEST_TARGET_BRANCH_NAME :
```
}}}
check against NULL:
```
CI_COMMIT_REF_NAME                  : cicd-1.3.0
CI_COMMIT_TAG                       : cicd-1.3.0
CI_PIPELINE_SOURCE                  : push
CI_DEFAULT_BRANCH                   : master
```


================================================================================


## push to branch
-----------------

O - codeQuality
O - cleanUpCodeQuality      (normally skipped)
O - test ; 72,73
O - cleanUpTest ; 72,73     (normally skipped)


## (UI) create MR
-----------------

O - codeQuality
O - cleanUpCodeQuality      (normally skipped)
O - test ; 72,73
O - cleanUpTest ; 72,73     (normally skipped)


## push to branch (with MR)
---------------------------

O - codeQuality
O - cleanUpCodeQuality      (normally skipped)
O - test ; 72,73
O - cleanUpTest ; 72,73     (normally skipped)


## (UI) merge
-------------

O - build
O - deploy
O - cleanUp                 (normally skipped)
O - stagingPre
O - staging


## (UI) create tag
------------------

O - build
O - deploy
O - cleanUp                 (normally skipped)
O - tagsDeployment


