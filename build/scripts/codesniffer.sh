#!/usr/bin/env sh

export REPO_DIR=$1

#echo "Execute composer install"
#composer install -o -q -d ${REPO_DIR}

if [[ -f ${REPO_DIR}/includes/vendor/bin/phpcs ]]; then
    echo "] start code quality test.."
    ${REPO_DIR}/includes/vendor/bin/phpcs -n\
        --extensions=php \
        --standard=${REPO_DIR}/phpcs-gitlab.xml \
        --exclude=PSR1.Methods.CamelCapsMethodName \
        --file-list=filelisting.txt \
        --report-info=${REPO_DIR}/code-quality-report.txt \
        --report-full

#    export codeQualityExitCode=$?

#    echo "Show code quality information"
#    ${REPO_DIR}/includes/vendor/bin/phpcs -n -q \
#        --extensions=php \
#        --standard=${REPO_DIR}/phpcs-gitlab.xml \
#        --exclude=PSR1.Methods.CamelCapsMethodName \
#        --report=info \
#        --file-list=filelisting.txt
#        #${REPO_DIR}

#    echo "Save code quality report"
#    ${REPO_DIR}/includes/vendor/bin/phpcs -n -q \
#        --extensions=php \
#        --standard=${REPO_DIR}/phpcs-gitlab.xml \
#        --exclude=PSR1.Methods.CamelCapsMethodName \
#        --file-list=filelisting.txt \
#        --report=info \
#        --report-file=${REPO_DIR}/code-quality-report.txt

#    if [[ ${codeQualityExitCode} -ne 0 ]]; then
#        exit 1
#    fi
else
    echo "] phpcs executable not found!"
    exit 1
fi
