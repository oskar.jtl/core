variables:
  GLOBAL_CI_REGISTRY_URL: "registry.gitlab.com/jtl-software/jtl-shop/core"
  GIT_DEPTH: 1

stages:
  - branchRun
  - branchSkip
  - buildRun
  - buildSkip
  - tagsOnly
  - everytime
  - codeQuality
  - cleanUpCodeQuality
  - test
  - cleanUpTest
  - build
  - deploy
  - cleanUp
  - tagsDeployment
  - stagingPre
  - staging
  - stagingMarketplaceDev

.codeQualityScripts: &codeQualityScripts |
  function codeQualityCheck() {
    export CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-code-quality-$CI_COMMIT_SHA

    echo "Login to GitLab Container Registry with CI credentials..."
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    echo ""
    echo "Code quality check of project in docker image codesniffer..."

    docker run \
      -v $(pwd):/code \
      -w /code \
      --name="$CI_CONTAINER_NAME" \
      $GLOBAL_CI_REGISTRY_URL/codesniffer \
      sh -c "bash build/scripts/global-codesniffer.sh /code"

    docker stop "$CI_CONTAINER_NAME"
    docker rm -fv "$CI_CONTAINER_NAME" >/dev/null

    echo "Code quality check finished"
  }

.testingScript: &testingScript |
  export CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-testing-$CI_COMMIT_SHA-php$PHP_VERSION_TEST_STEP

  function testing() {
    echo "Login to GitLab Container Registry with CI credentials..."
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    echo ""
    echo "Testing project in PHP $PHP_VERSION_TEST_STEP docker image..."

    docker run \
      -w '/creation' \
      --volume $(pwd):/dockerBuild \
      --name="$CI_CONTAINER_NAME" \
      $GLOBAL_CI_REGISTRY_URL/testing-php$PHP_VERSION_TEST_STEP \
      sh -c "cp -r /dockerBuild/. /creation; bash build/scripts/tests.sh"

    docker stop "$CI_CONTAINER_NAME"
    docker rm -fv "$CI_CONTAINER_NAME" >/dev/null

    echo "Tests finished"
  }

.cleanUpTesting: &cleanUpTesting |
  function failedCodeQuality() {
    CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-code-quality-$CI_COMMIT_SHA
    running=$(docker inspect -f '{{.State.Running}}' $CI_CONTAINER_NAME)

    if [[ ! -z "$running" ]]; then
      docker stop $CI_CONTAINER_NAME && docker rm -fv $CI_CONTAINER_NAME
    fi
  }
  function failedTesting() {
    CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-testing-$CI_COMMIT_SHA-php$PHP_VERSION_TEST_STEP
    running=$(docker inspect -f '{{.State.Running}}' $CI_CONTAINER_NAME)

    if [[ ! -z "$running" ]]; then
      docker stop $CI_CONTAINER_NAME && docker rm -fv $CI_CONTAINER_NAME
    fi
  }

.preBuildDeployScripts: &preBuildDeployScripts |
  export CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-build-deploy-$CI_COMMIT_SHA
  export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
  export BUILD_SCRIPT_PATH=build/scripts/build.sh
  export DEPLOY_SCRIPT_PATH=build/scripts/deploy.sh

  function dockerLogin() {
    echo "Login to GitLab Container Registry with CI credentials..."
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    echo ""
  }
  function buildProcess() {
    RAND_CHAR=$(date +%s|sha256sum|head -c 6)
    DB_NAME="build_$RAND_CHAR"
    DB_HOST_NET=$(docker network inspect -f '{{range .Containers}}{{if eq .Name "mariadb-10-2-container"}}{{.IPv4Address}}{{end}}{{end}}' mysql-php-network)
    DB_HOST="${DB_HOST_NET::-3}"

    echo "Building projekt in PHP 7.1 and mariadb server 10.2 docker image..."
    docker run \
      -d -it \
      -w '/creation' \
      --volume $CI_PROJECT_DIR.tmp/CI_SERVER_TLS_CA_FILE:$CI_PROJECT_DIR.tmp/CI_SERVER_TLS_CA_FILE \
      --volume $(pwd):/dockerBuild \
      --name="$CI_CONTAINER_NAME" \
      --network mysql-php-network \
      $GLOBAL_CI_REGISTRY_URL/build-deploy \
      bash

    docker exec \
      -w '/creation' \
      $CI_CONTAINER_NAME \
      sh -c "cp -r /dockerBuild/. /creation; bash $BUILD_SCRIPT_PATH '/creation' $CI_COMMIT_REF_NAME $CI_COMMIT_SHA '$DB_HOST' 'root' '$DOCKER_MARIADB_PASSWORD' 'build_$DB_NAME'"

    echo "Build finished"
  }
  function deployProcess() {
    echo "Deploying archive to build server..."
    docker start $CI_CONTAINER_NAME

    docker exec \
      -w '/creation' \
      $CI_CONTAINER_NAME \
      /bin/bash -c "bash $DEPLOY_SCRIPT_PATH $CI_PROJECT_NAME $CI_COMMIT_REF_NAME /archive"

    docker cp $CI_CONTAINER_NAME:/archive/. $CI_BUILD_SERVER_ARCHIVE_PATH
    docker network disconnect mysql-php-network $CI_CONTAINER_NAME
    docker stop "$CI_CONTAINER_NAME"
    docker rm -fv "$CI_CONTAINER_NAME" >/dev/null
  }

.postBuildDeployScripts: &postBuildDeployScripts |
  function dockerLogout() {
    echo "Logout from GitLab Container Registry with CI credentials..."
    docker logout "$CI_REGISTRY"
    echo ""
  }

.cleanUpScript: &cleanUpScript |
  function failedScript() {
    CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-build-deploy-$CI_COMMIT_SHA
    running=$(docker inspect -f '{{.State.Running}}' $CI_CONTAINER_NAME)

    if [[ ! -z "$running" ]]; then
      docker network disconnect mysql-php-network $CI_CONTAINER_NAME
      docker stop $CI_CONTAINER_NAME && docker rm -fv $CI_CONTAINER_NAME
    fi
  }

.devCheckoutScripts: &devCheckoutScripts |
  export FILENAME="${CI_COMMIT_REF_NAME//[\/\.]/-}";
  export EXTRACTION_PATH="$RUNNER_PATH/shop-$FILENAME";

  function downloadProcess() {
    RUNNER_PATH_ZIP="$EXTRACTION_PATH.zip";
    DOWNLOAD_URL="https://build.jtl-shop.de/get/shop-$FILENAME.zip";

    curl -o $RUNNER_PATH_ZIP $DOWNLOAD_URL
    unzip -qo $RUNNER_PATH_ZIP -d $EXTRACTION_PATH
    rm $RUNNER_PATH_ZIP
  }
  function deployProcess() {
    PHP_VERSION=$1;
    DEPLOY_PATH="$STAGING_USER_PATH/php$PHP_VERSION/$FILENAME";
    MIGRATION_COUNT_BEFORE=$(ls -1 $DEPLOY_PATH/update/migrations/ | wc -l);

    sudo /usr/bin/rsync -rt -og --chown=www-data:www-data $EXTRACTION_PATH/. $DEPLOY_PATH;

    MIGRATION_COUNT_AFTER=$(ls -1 $DEPLOY_PATH/update/migrations/ | wc -l);

    if [[ $MIGRATION_COUNT_AFTER -gt $MIGRATION_COUNT_BEFORE ]]; then
      php -r "
      require_once '$DEPLOY_PATH/includes/globalinclude.php'; \
      \$time    = date('YmdHis'); \
      \$manager = new MigrationManager(Shop::Container()->getDB()); \
      try { \
          \$migrations = \$manager->migrate(\$time); \
          foreach (\$migrations as \$migration) { \
              echo \$migration->getName().' '.\$migration->getDescription().PHP_EOL; \
          } \
      } catch (Exception \$e) { \
          \$migration = \$manager->getMigrationById(array_pop(array_reverse(\$manager->getPendingMigrations()))); \
          \$result    = new IOError('Migration: '.\$migration->getName().' | Errorcode: '.\$e->getMessage()); \
          echo \$result->message; \
          return 1; \
      } \
      ";
    fi
  }

.marketplaceDevScripts: &marketplaceDevScripts |
  export FILENAME="${CI_COMMIT_REF_NAME//[\/\.]/-}";
  export EXTRACTION_PATH="$RUNNER_PATH/shop-$FILENAME";

  function downloadProcess() {
    RUNNER_PATH_ZIP="$EXTRACTION_PATH.zip";
    DOWNLOAD_URL="https://build.jtl-shop.de/get/shop-$FILENAME.zip";

    curl -o $RUNNER_PATH_ZIP $DOWNLOAD_URL
    unzip -qo $RUNNER_PATH_ZIP -d $EXTRACTION_PATH
    rm $RUNNER_PATH_ZIP
  }
  function deployProcess() {
    if [[ -d "$STAGING_MARKETPLACE_PATH/update/migrations/" ]]; then
      MIGRATION_COUNT_BEFORE=$(ls -1 $STAGING_MARKETPLACE_PATH/update/migrations/ | wc -l);

      sudo /usr/bin/rsync -rt -og --chown=nginx:nginx $EXTRACTION_PATH/. $STAGING_MARKETPLACE_PATH

      MIGRATION_COUNT_AFTER=$(ls -1 $STAGING_MARKETPLACE_PATH/update/migrations/ | wc -l);

      if [[ $MIGRATION_COUNT_AFTER -gt $MIGRATION_COUNT_BEFORE ]]; then
        php -r "
        require_once '$STAGING_MARKETPLACE_PATH/includes/globalinclude.php'; \
        \$time    = date('YmdHis'); \
        \$manager = new MigrationManager(Shop::Container()->getDB()); \
        try { \
            \$migrations = \$manager->migrate(\$time); \
            foreach (\$migrations as \$migration) { \
                echo \$migration->getName().' '.\$migration->getDescription().PHP_EOL; \
            } \
        } catch (Exception \$e) { \
            \$migration = \$manager->getMigrationById(array_pop(array_reverse(\$manager->getPendingMigrations()))); \
            \$result    = new IOError('Migration: '.\$migration->getName().' | Errorcode: '.\$e->getMessage()); \
            echo \$result->message; \
            return 1; \
        } \
        ";
      fi
    else
      sudo /usr/bin/rsync -rt -og --chown=nginx:nginx $EXTRACTION_PATH/. $STAGING_MARKETPLACE_PATH
    fi
  }



.debugging: &debugging
  script:
    - if [ $CI_COMMIT_BRANCH ] ; then echo 'CI_COMMIT_BRANCH :' $CI_COMMIT_BRANCH; fi;                                                          # --DEBUG--
    - if [ $CI_COMMIT_REF_NAME ] ; then echo 'CI_COMMIT_REF_NAME :' $CI_COMMIT_REF_NAME; fi;                                                    # --DEBUG--
    - if [ $CI_MERGE_REQUEST_IID ] ; then echo 'CI_MERGE_REQUEST_IID :' $CI_MERGE_REQUEST_IID; fi;                                              # --DEBUG--
    - if [ $CI_MERGE_REQUEST_ID ] ; then echo 'CI_MERGE_REQUEST_ID :' $CI_MERGE_REQUEST_ID; fi;                                                 # --DEBUG--
    - if [ $CI_COMMIT_TAG ] ; then echo 'CI_COMMIT_TAG :' $CI_COMMIT_TAG; fi;                                                                   # --DEBUG--
    - if [ $CI_PIPELINE_SOURCE ] ; then echo 'CI_PIPELINE_SOURCE :' $CI_PIPELINE_SOURCE; fi;                                                    # --DEBUG--
    - if [ $CI_MERGE_REQUEST_EVENT_TYPE ] ; then echo 'CI_MERGE_REQUEST_EVENT_TYPE :' $CI_MERGE_REQUEST_EVENT_TYPE; fi;                         # --DEBUG--
    - if [ $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME ] ; then echo 'CI_MERGE_REQUEST_SOURCE_BRANCH_NAME :' $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME; fi; # --DEBUG--
    - if [ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME ] ; then echo 'CI_MERGE_REQUEST_TARGET_BRANCH_NAME :' $CI_MERGE_REQUEST_TARGET_BRANCH_NAME; fi; # --DEBUG--
    - if [ $CI_DEFAULT_BRANCH ] ; then echo 'CI_DEFAULT_BRANCH :' $CI_DEFAULT_BRANCH; fi;                                                       # --DEBUG--


# -------------------------------------------------------------------------------- --DEVELOPMENT--BEGIN
# {{{

.RUN only branches:
  stage: branchRun
  image: alpine:latest
  script:
#    - echo "trigger error" | grep "foo"    # --DEBUG--
  <<: *debugging
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_TAG == null
      when: always

.RUN only branches skiped:
  stage: branchSkip
  image: alpine:latest
  script:
  <<: *debugging
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_TAG == null
      when: on_failure


.RUN only build:
  stage: buildRun
  image: alpine:latest
  script:
#    - echo "trigger error" | grep "foo"    # --DEBUG--
  <<: *debugging
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
        || $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME

.RUN only build skiped:
  stage: buildSkip
  image: alpine:latest
  script:
  <<: *debugging
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
        || $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME
      when: on_failure


.RUN tags only:
  stage: tagsOnly
  image: alpine:latest
  script:
  <<: *debugging
  rules:
    # runs not on branches, but tags
    - if: $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME
    - if: $CI_COMMIT_TAG == ''
      when: never


.RUN always:
  stage: everytime
  image: alpine:latest
  script:
  <<: *debugging

# }}}
# -------------------------------------------------------------------------------- --DEVELOPMENT--END

# rules for ALL pipellines
# prevent running of merge request pipelines
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: always




#.CodeSnifferInstallScript: &CodeSnifferInstallScript
#  before_script:
#    - export REPODIR=$(pwd)
#    - echo "] installing composer.."
#    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#    - php -r "if (hash_file('sha384', 'composer-setup.php') === file_get_contents('https://composer.github.io/installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
#    - php composer-setup.php && rm composer-setup.php
#    - mv composer.phar composer
#    - echo "] remove default composer files.."
#    - cd includes/ && rm composer.json composer.lock
#    - echo "] run phpcs install.."
#    - ../composer -q require squizlabs/php_codesniffer
#    - cd "$REPODIR"
#
#.CodeSnifferRunScript: &CodeSnifferRunScript
#  script:
#    # create the phpcs config
#    - php -r '$conf=["show_progress"=>"1","colors"=>"1","report_width"=>"90","encoding"=>"utf-8"];file_put_contents("includes/vendor/squizlabs/php_codesniffer/CodeSniffer.conf","<?php \$phpCodeSnifferConfig=".var_export($conf,true)."; ?>");'
#    # create file list
#    - find . -iname '*.php' >filelisting.txt
#    # run the sniffer script (he uses the global script, but "build/scripts/global-codesniffer.sh" seems better)
#    - build/scripts/codesniffer.sh .

.ComposerInstall: &ComposerInstall |
  # COMPOSER INSTALLATION
  echo "] installing composer.."
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  php -r "if (hash_file('sha384', 'composer-setup.php') === file_get_contents('https://composer.github.io/installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
  php composer-setup.php && rm composer-setup.php
  mv composer.phar composer


.CodeSnifferInstallScript: &CodeSnifferInstallScript |
  # CODESNIFFER INSTALLATION
  echo "] remove default composer files.."
  rm includes/composer.json includes/composer.lock
  echo "] running phpcs install.."
  ./composer -q require squizlabs/php_codesniffer -d includes/

.CodeSnifferRunScript: &CodeSnifferRunScript |
  # CODESNIFFER RUN
  echo "] writing phpcs config.."
  php -r '$conf=["show_progress"=>"1","colors"=>"1","report_width"=>"90","encoding"=>"utf-8"];file_put_contents("includes/vendor/squizlabs/php_codesniffer/CodeSniffer.conf","<?php \$phpCodeSnifferConfig=".var_export($conf,true)."; ?>");'
  echo "] create file list.."
  find . -iname '*.php' >filelisting.txt
  echo "] starting check script.."
  build/scripts/codesniffer.sh .


code_quality:
  stage: codeQuality
  image: php:7.3-alpine
  before_script:
    - *ComposerInstall
    - *CodeSnifferInstallScript
  script:
    - *CodeSnifferRunScript
  artifacts:
    when: always
    paths: [code-quality-report.txt]
  rules:
    # run only on branches, not master, not tags
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
        && $CI_COMMIT_TAG == null
      when: always



# {{{                                CLEAN UP not needed anymore
.removeContainerCodeQuality:
  stage: cleanUpCodeQuality
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    GIT_STRATEGY: none
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *cleanUpTesting
    - failedCodeQuality
  <<: *debugging
  rules:
    # run only on branches, not master, not tags
    # when previouse job failed
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
        && $CI_COMMIT_TAG == null
      when: on_failure
###    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
###      when: never
###    - if: $CI_MERGE_REQUEST_ID != null
###      when: never
###    - if: $CI_COMMIT_TAG != null
###      when: never
###    - if: $CI_COMMIT_REF_NAME !~ /^release\/.*$/
###      when: never
###    - when: never
###    - when: on_failure
#  only:
#    - branches
#  except:
#    - master
#    - /^release\/.*$/
#  when: on_failure
# }}}


.unitTestsInstallScript: &unitTestsInstallScript |
  # INSTALLING SYSTEM DEPENDENCIES
  apk update
  echo -n "] system package install.."
  apk add --no-cache freetype-dev g++ git gnupg icu-dev libjpeg-turbo-dev libpng-dev libxml2-dev libzip-dev php7-bcmath php7-gd php7-intl php7-soap php7-zip unzip zip zlib-dev >/dev/null
  apk add --update nodejs nodejs-npm >/dev/null
  if [ $? -gt 0 ] ; then echo -e "\e[1;31m run with ERRORs\e[0m"; else echo -e "\e[1;32m OK\e[0m"; fi
  echo "] installing PHP extensions:"
  echo -n "] bcmath.."
  docker-php-ext-install -j$(nproc) bcmath &>/dev/null; if [ $? -gt 0 ] ; then echo -e "\e[1;31m run with ERRORs\e[0m"; else echo -e "\e[1;32m OK\e[0m"; fi
  echo -n "] gd.."
  docker-php-ext-install -j$(nproc) gd &>/dev/null; if [ $? -gt 0 ] ; then echo -e "\e[1;31m run with ERRORs\e[0m"; else echo -e "\e[1;32m OK\e[0m"; fi
  echo -n "] intl.."
  docker-php-ext-install -j$(nproc) intl &>/dev/null; if [ $? -gt 0 ] ; then echo -e "\e[1;31m run with ERRORs\e[0m"; else echo -e "\e[1;32m OK\e[0m"; fi
  echo -n "] soap.."
  docker-php-ext-install -j$(nproc) soap &>/dev/null; if [ $? -gt 0 ] ; then echo -e "\e[1;31m run with ERRORs\e[0m"; else echo -e "\e[1;32m OK\e[0m"; fi
  echo -n "] zip.."
  docker-php-ext-install -j$(nproc) zip &>/dev/null; if [ $? -gt 0 ] ; then echo -e "\e[1;31m run with ERRORs\e[0m"; else echo -e "\e[1;32m OK\e[0m"; fi

.unitTestsRunScript: &unitTestsRunScript |
  # RUNNING UNIT TESTS
  echo "] start test script.."
  build/scripts/tests.sh


.runUnitTestDocker: &runUnitTestDocker |
  # RUNNING UNIT TESTS
  # docker run --rm -v $CI_PROJECT_DIR:/repofolder registry.gitlab.com/oskar.jtl/core/ci-tests:php72 /repofolder/build/scripts/tests.sh
  echo "IMAGE NAME:"${IMAGE_NAME}
  docker run \
    --rm \
    --name ci_unittests_${CI_RUNNER_SHORT_TOKEN}_${CI_COMMIT_SHORT_SHA} \
    -v $CI_PROJECT_DIR:/repofolder registry.gitlab.com/oskar.jtl/core/${IMAGE_NAME} \
    /repofolder/build/scripts/tests.sh


test_PHP_7.2:
  stage: test
  #image: pulled from docker.hub via runner executor
  before_script:
    #- *unitTestsInstallScript
    #- echo "DEBUG.."
    - echo "CI_PROJECT_PATH :" $CI_PROJECT_PATH               # for prevent runs on forks
    - echo "CI_RUNNER_SHORT_TOKEN :" $CI_RUNNER_SHORT_TOKEN   # that's it! maybe...
    #- echo "CI_RUNNER_ID :"$CI_RUNNER_ID
  script:
    #- *ComposerInstall
    #- *unitTestsRunScript
    #- docker run --rm -v $CI_PROJECT_DIR:/repofolder tron5/ci-testing:php72 /repofolder/build/scripts/tests.sh
    - IMAGE_NAME=ci-tests:php72
    - *runUnitTestDocker
#  <<: *debugging
  rules:
    # run only on branches, not master, not tags ... and only in our repo!
    - if: $CI_PROJECT_PATH =~ /^oskar.*$/
        && $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
        && $CI_COMMIT_TAG == null
      when: on_success
  tags:
    - tests-run-72


test_PHP_7.3:
  stage: test
  #image: php:7.3-alpine
  before_script:
    #- *unitTestsInstallScript
  script:
    #- *ComposerInstall
    #- *unitTestsRunScript
    #- docker run --rm -v $CI_PROJECT_DIR:/repofolder tron5/ci-testing:php73 /repofolder/build/scripts/tests.sh
    #- docker run --rm -v $CI_PROJECT_DIR:/repofolder registry.gitlab.com/oskar.jtl/core/ci-tests:php73 /repofolder/build/scripts/tests.sh
    - IMAGE_NAME=ci-tests:php72
    - *runUnitTestDocker
#  <<: *debugging
  rules:
    # run only on branches, not master, not tags ... and only in our repo!
    - if: $CI_PROJECT_PATH =~ /^oskar.*$/
        && $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
        && $CI_COMMIT_TAG == null
      when: on_success
  tags:
    - tests-run-73

test_PHP_7.4:
  stage: test
  #image: php:7.4-alpine
  before_script:
    #- *unitTestsInstallScript
  script:
    #- *ComposerInstall
    #- *unitTestsRunScript
    #- docker run --rm -v $CI_PROJECT_DIR:/repofolder tron5/ci-testing:php74 /repofolder/build/scripts/tests.sh
    #- docker run --rm -v $CI_PROJECT_DIR:/repofolder registry.gitlab.com/oskar.jtl/core/ci-tests:php74 /repofolder/build/scripts/tests.sh
    - IMAGE_NAME=ci-tests:php72
    - *runUnitTestDocker
#  <<: *debugging
  rules:
    # run only on branches, not master, not tags ... and only in our repo!
    - if: $CI_PROJECT_PATH =~ /^oskar.*$/
        && $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
        && $CI_COMMIT_TAG == null
      when: on_success
  tags:
    - tests-run-74


# {{{                                   OFF and no more needed

      #test_PHP_7.3:
      #  stage: test
      #  image: docker:19.03.1
      #  services:
      #    - docker:19.03.1-dind
      #  variables:
      #    PHP_VERSION_TEST_STEP: '7.3'
      #    DOCKER_DRIVER: overlay2
      #    DOCKER_TLS_CERTDIR: "/certs"
      #  script:
      #    - *testingScript
      #    - testing
      #  <<: *debugging
      #  rules:
      #    # run only on branches, not master, not tags
      #    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      #        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
      #        && $CI_COMMIT_TAG == null
      #      when: always
      ##  only:
      ##    - branches
      ##  except:
      ##    - master


.removeContainerPHP7.2:
  stage: cleanUpTest
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    GIT_STRATEGY: none
    PHP_VERSION_TEST_STEP: '7.2'
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *cleanUpTesting
    - failedTesting
  <<: *debugging
  rules:
    # run only on branches, not master, not /release*/, not tags
    # when last job failed
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
        && $CI_COMMIT_TAG == null
      when: on_failure
###    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
###      when: never
###    - if: $CI_COMMIT_BRANCH =~ /^release\/.*$/
###      when: never
###    - when: on_failure
#  only:
#    - branches
#  except:
#    - master
#    - /^release\/.*$/
#  when: on_failure

.removeContainerPHP7.3:
  stage: cleanUpTest
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    GIT_STRATEGY: none
    PHP_VERSION_TEST_STEP: '7.3'
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *cleanUpTesting
    - failedTesting
  <<: *debugging
  rules:
    # run only on branches, not master, not /release*/, not tags
    # when last job failed
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        && $CI_COMMIT_REF_NAME !~ /^release\/.*$/
        && $CI_COMMIT_TAG == null
      when: on_failure
###    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
###      when: never
###    - if: $CI_COMMIT_BRANCH =~ /^release\/.*$/
###      when: never
###    - when: on_failure
#  only:
#    - branches
#  except:
#    - master
#    - /^release\/.*$/
#  when: on_failure

# }}}



build:
  stage: build
  before_script:
      # - *preBuildDeployScripts
      # - dockerLogin
    - echo "before-script"
    #- echo "trigger error" | grep "foo"    # --DEBUG--
  script:
      # - buildProcess
      # - echo "script"
  <<: *debugging
  after_script:
      # - *postBuildDeployScripts
      # - dockerLogout
    - echo "after-script"
  rules:
    # runs on master, /release*/ and tags
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
        || $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME
###    - if: $CI_MERGE_REQUEST_ID
###      when: never
###    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
###    - if: $CI_COMMIT_BRANCH =~ /^release\/.*$/
###    - if: $CI_COMMIT_TAG
#  only:
#    - master
#    - /^release\/.*$/
#    - tags
#  tags:
#  - build

deploy:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  before_script:
      #- *preBuildDeployScripts
      #- dockerLogin
    - echo "before-script"
#    - echo "trigger error" | grep "foo"    # --DEBUG--
  script:
      #- deployProcess
      #- buildscript pipeline:shop5up "$CI_PROJECT_DIR" "$CI_COMMIT_REF_NAME" "$CI_COMMIT_SHA"
    - echo "script"
  <<: *debugging
  after_script:
      #- *postBuildDeployScripts
      #- dockerLogout
    - echo "after-script"
  rules:
    # runs on master, /release*/ and tags
    # if the previouse job succeeded
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
        || $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME
      when: on_success
###    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
###      when: never
###    - if: $CI_MERGE_REQUEST_ID
###      when: never
###    - if: $CI_COMMIT_REF_NAME =~ /^release\/.*$/
###    - if: $CI_COMMIT_TAG
###    - when: on_success
#  only:
#    - master
#    - /^release\/.*$/
#    - tags
#  when: on_success
#  tags:
#    - build

removeContainer:
  stage: cleanUp
  script:
      #- *cleanUpScript
      #- failedScript
    - echo "script"
  <<: *debugging
  rules:
    # runs on master, /release*/ and tags
    # if the previouse job (deploy) was failed
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
        || $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME
      when: on_failure
###    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
###      when: never
###    - when: on_failure
###    - if: $CI_MERGE_REQUEST_ID == null
###      when: never
###    - if: $CI_COMMIT_TAG == null
###      when: never
###    - if: $CI_COMMIT_REF_NAME =~ /^release\/.*$/
###    - when: on_failure
#  only:
#    - master
#    - /^release\/.*$/
#    - tags
#  when: on_failure
#  tags:
#    - build


tagsDeployment:
  stage: tagsDeployment
  variables:
    GIT_STRATEGY: none
  script:
      #- buildscript release:deployment "$CI_BUILD_SERVER_ARCHIVE_PATH"
    - echo "script"
  <<: *debugging
  rules:
    # runs not on branches, but tags
###    - if: $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME
###    - if: $CI_COMMIT_TAG == ''
###      when: never
    - if: $CI_COMMIT_TAG == $CI_COMMIT_REF_NAME
      when: manual
    - when: never
#  except:
#    - branches
#  only:
#    - tags
#  tags:
#    - build



stagingPreDeploy:
  stage: stagingPre
  variables:
    GIT_STRATEGY: none
  script:
      # - *devCheckoutScripts
      # - downloadProcess
    - echo "script"
  <<: *debugging
  rules:
    # runs only on master ("default branch")
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
#  only:
#    - master
#  tags:
#    - dev-checkout

stagingDevPhp72:
  stage: staging
  variables:
    GIT_STRATEGY: none
  script:
      # - *devCheckoutScripts
      # - deployProcess 72
    - echo "script"
  <<: *debugging
  environment:
    name: staging master php 7.2
    url: https://$STAGING_USER-72-master.$STAGING_DOMAIN
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
#  only:
#    - master
#  tags:
#    - dev-checkout

stagingDevPhp73:
  stage: staging
  variables:
    GIT_STRATEGY: none
  script:
      # - *devCheckoutScripts
      # - deployProcess 73
    - echo "script"
  <<: *debugging
  environment:
    name: staging master php 7.3
    url: https://$STAGING_USER-73-master.$STAGING_DOMAIN
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
        || $CI_COMMIT_REF_NAME =~ /^release\/.*$/
#  only:
#    - master
#  tags:
#    - dev-checkout



# {{{
.stagingMarketplaceDev:
  stage: stagingMarketplaceDev
  variables:
    GIT_STRATEGY: none
  script:
      # - *marketplaceDevScripts
      # - downloadProcess
      # - deployProcess
    - echo "script"
  <<: *debugging
  environment:
    name: staging marketplace dev
    url: https://$STAGING_MARKETPLACE_DOMAIN
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
#  only:
#    - master
#  tags:
#    - test-marketplace
# }}}
