��    O      �  k         �  	   �  	   �     �     �     �               0     C     S     k     �     �     �     �     �     �     �          0     D     U     k     ~     �     �     �     �     �     �     	     	     0	     C	     O	  
   U	     `	     h	     u	     �	     �	     �	     �	     �	     �	     �	     
     
     
     $
     )
     1
  
   A
     L
     S
     _
     o
     w
     |
     �
     �
     �
     �
     �
     �
     �
     �
     �
          2     M     [     d     i  	   n  
   x  
   �     �  (   �  	   �     �     �     �     	       	   (  &   2     Y     _  
   w     �     �     �     �  +   �     �     �  
     &        C     H     d  !   l     �     �     �     �  	   �     �     �          8     R     _     f  	   v     �  *   �  "   �  A   �  :   !  -   \  '   �     �  "   �     �     �                    #  
   8     C     J  �   P     "     1     =     U     r     z  	   �     �  )   �  #   �  %   �  0     2   B  1   u     �  	   �     �     �     �     �     �     �               6   '      I   "   $   C               7          /   5      <   O       F                     8   1      +           %       .   =   &                         ?          N                     B              :       (      A       ;             J   )   *       -   0   
   #          G   ,   2          3   M      >          9   K          H      @       4   L   !       	      E   D    benchmark busySpace cacheURL cg_article_description cg_article_nicename cg_attribute_description cg_attribute_nicename cg_box_description cg_box_nicename cg_category_description cg_category_nicename cg_core_description cg_core_nicename cg_filter_description cg_filter_nicename cg_language_description cg_language_nicename cg_manufacturer_description cg_manufacturer_nicename cg_news_description cg_news_nicename cg_object_description cg_object_nicename cg_option_description cg_option_nicename cg_plugin_description cg_plugin_nicename cg_template_description cg_template_nicename clearObjectCache clearPageCache clearTemplateCache configurationError description empty emptySpace entries entriesCount errorBenchmark errorCacheDelete errorCacheMethodSelect errorCacheTypeDelete errorDirDelete errorFileDelete errorMethodNotFound errorNoCacheType filesFrontend fullSize hitRate hits inserts keyCountInCache management misses objectcache objectcacheDesc repeats runs scriptCountInCache showAdvanced slowlog startBenchmark stats status successCacheDelete successCacheEmptied successCacheMethodSave successCacheTypeActivate successCacheTypeDeactivate successTemplateCacheDelete templateCache testData time type typeArray typeObject typeString uptime Content-Type: text/plain; charset=UTF-8
 Benchmark Belegter Speicher https://jtl-url.de/lt1ec Enthält Artikeldaten Artikel Enthält Attributdaten Attribute Enthält Boxen und deren Einstellungen Boxen Enthält Kategoriedaten Kategorien Enthält JTL-eigene Daten Core Enthält Filterdaten Artikelfilter Enthält Sprachvariablen und Übersetzungen Sprache Enthält Herstellerdaten Hersteller Enthält Blogbeiträge und -kategorien Blog Enthält allgemeine Objekte Objekte Enthält allgemeine Einstellungen Optionen Enthält Plugin-Daten Plugins Enthält Template-Einstellungen Templates Gesamten Objekt-Cache leeren Gesamten Seiten-Cache leeren Gesamten Template-Cache leeren (fehlerhaft konfiguriert) Beschreibung leeren Freier Speicher Einträge Anzahl Einträge Benchmark konnte nicht ausgeführt werden. Cache konnte nicht geleert werden. Es konnte keine funktionierende Cache-Methode ausgewählt werden. Cache %s konnte nicht geleert werden (evtl. bereits leer). Verzeichnis %s konnte nicht gelöscht werden! Datei %s konnte nicht gelöscht werden! Keine Methoden gefunden. Bitte wählen Sie einen Cache-Typ. Dateien Frontend Gesamtgröße Hit-Rate Hits Inserts Anzahl Keys im Cache Verwaltung Misses Cache Hier stellen Sie den von JTL-Shop genutzten Cache ein. Der Objekt-Cache puffert Daten, damit sie schneller geladen werden. Sie können für jede Objektart aktivieren, ob diese im Cache gespeichert werden soll. Wiederholungen Durchläufe Anzahl Skripte im Cache Erweiterte Optionen anzeigen Slowlog Benchmark starten Statistik Status Object-Cache wurde erfolgreich gelöscht.  Caches wurden erfolgreich geleert.  wurde als Cache-Methode gespeichert. Ausgewählte wurden Typen erfolgreich aktiviert. Ausgewählte Typen wurden erfolgreich deaktiviert. Es wurden %d Dateien im Template-Cache gelöscht. Template-Cache Testdaten Zeiten Typ Array Objekt String Uptime 